import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-acceptedleads',
  templateUrl: './acceptedleads.component.html',
  styleUrls: ['./acceptedleads.component.css']
})
export class AcceptedleadsComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error :any;
  allLeadsDetail : any;
  leadsDetailsById : any ={category:null, brand:null,description:null,date:null};
  customerAddressByLeadId : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  l_id:any;
  service_team : any = {name:null, email:null, st_phone:null, photo:null};
  //public tableData1: TableData;
  public showDialog : boolean;
  //public showDialogUpdate : boolean;
  public showDialogFollowUp:boolean;
  followupmsg : any;
  image : any;
  audio:any;
  c_phone:any;
  st_phone:any;
  @ViewChild('fileImg') myInputVariableImage: any;
  msg:any;
  @ViewChild('fileAudio') myInputVariableAudio:any;
  followUpDate:any;
  status:any;
  showDialogCompleted:boolean;
  totalAmount:any;
  billAmount:any;
  gst:any;
  billDescription:any;
  minDate: any;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.getAcceptedLeads();
    this.showDialog = false;
    this.minDate = this.formatDate(new Date());
    //this.showDialogUpdate = false;
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  passAmount(){
    console.log('passAmount');
    if(this.gst){
      console.log('yes');
      this.totalAmount = this.billAmount *18/100 + parseInt(this.billAmount);
    }
    else{
      console.log('nos');
      this.totalAmount = this.billAmount;
    }
  }

  gstCalculate(){
    console.log('gst');
    if(this.billAmount != null){
      if(this.gst){
        console.log('yes');
        this.totalAmount = this.billAmount *18/100 + parseInt(this.billAmount);
      }
      else{
        console.log('nos');
        this.totalAmount = this.billAmount;
      }   
    }
  }

  public completed(l_id,st_phone,c_phone){
    this.showDialogCompleted = true;
    this.status = 'COMPLETED';
    this.l_id = l_id;
    this.c_phone = c_phone;
    this.st_phone = st_phone;
  }

  public fileChanged(event){
    this.image = event;
     console.log(this.image);
  }

  public fileChanged1(event){
    this.audio = event;
     console.log(this.audio);
  }

  public followUpInfo(){
        let formData:FormData = new FormData();
        let fileList1;
          if(this.image){
             fileList1 = this.image.target.files;
             for(var i=0;i<fileList1.length;i++){
             formData.append('images[]', fileList1[i], fileList1[i].name);
           }
          }
          if(this.audio){
              let aud = this.audio.target.files;
              formData.append('audioo', aud[0]);
          }
          if(this.followUpDate==null){
            return alert('followUp Date Not Choosen ');
          }
        formData.append('status',this.status);
        formData.append('msg',this.msg);
        formData.append('followUpDate',this.followUpDate);
        formData.append('l_id', this.l_id);
        formData.append('c_phone', this.c_phone);
        formData.append('st_phone',this.st_phone);
        if(this.status == 'COMPLETED'){
          if(this.billDescription == null){
            alert('Please Enter Bill Details');
            return;
          }
          if(this.billAmount==null){
            alert('Please Enter Bill Amount');
            return;
          }
          formData.append('billDescription', this.billDescription);
          formData.append('billAmount', this.billAmount);
          formData.append('gst',this.gst);
        }

        let headers = new Headers({
          'Accept':  'application/json',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        });
        let options = new RequestOptions({ headers: headers });
        this.http.post('http://'+this.url+'addFollowUpInfoAdmin', formData, options).map(res => res.json()).catch(error => Observable.throw(error)).subscribe(
                data => {
                  if(data.sessionExpire){
                      this.router.navigate(['/login']);
                      alert(data.message);
                    }
                  else{
                    if(data.error){
                      alert(data.message);
                      //this.st_details = null;
                    }
                    else{
                      alert(data.message);
                      this.msg=null;
                      this.followUpDate=null;
                      this.myInputVariableImage.nativeElement.value = "";
                      this.myInputVariableAudio.nativeElement.value = "";
                      this.totalAmount=null;
                      this.billAmount = null;
                      this.billDescription =null;
                      //this.getFollowUpInfo(this.l_id);
                      this.getAcceptedLeads();
                      this.showDialogFollowUp = false;
                      this.showDialogCompleted = false;

                    }
                  }

                },
                error => console.log(error)
            )
  }

  public followUp(l_id,st_phone,c_phone){
    this.showDialogFollowUp = true;
    this.status = 'OPEN';
    this.l_id = l_id;
    this.c_phone = c_phone;
    this.st_phone = st_phone;
    //this.getFollowUpInfo(l_id);
  }

  public getFollowUpInfo(l_id){
      this.data = "l_id="+l_id;

      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getFollowUpInfoAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.followupmsg=data.msg;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
  }

  public getAcceptedLeads(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'getAllAcceptedLeadsAdmin',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.allLeadsDetail=null;
        }
        else{
          this.allLeadsDetail=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }

    });
  }

  public viewLead(l_id){
    this.l_id = l_id;
    this.data = "l_id="+l_id;
    this.showDialog = true;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            //alert(data.message);
            //console.log(data);
            this.leadsDetailsById = data.lead_data[0];
            this.customerAddressByLeadId = data.customer_address[0];
            this.getServiceTeamInfoByLeadId();
            this.getFollowUpInfo(l_id);

          };
        }

    });
  }

  public getServiceTeamInfoByLeadId(){
    //this.category = this.leadsDetailsById.category;
    //this.data = "category="+this.leadsDetailsById.category;
    this.data = "l_id="+this.leadsDetailsById.l_id;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'getServiceTeamInfoByLeadIdAcceptedAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.service_team = data.service_team_info[0];
            console.log(data);
          };
        }

    });
  }

}
