import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptedleadsComponent } from './acceptedleads.component';

describe('AcceptedleadsComponent', () => {
  let component: AcceptedleadsComponent;
  let fixture: ComponentFixture<AcceptedleadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptedleadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptedleadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
