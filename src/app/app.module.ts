import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import {HashLocationStrategy, LocationStrategy} from "@angular/common";


import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
import { NguiMapModule} from '@ngui/map';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { UserComponent }   from './user/user.component';
import { DialogComponent } from './dialog/dialog.component';
import { LoginComponent } from './login/login.component';
import { LeadsComponent } from './leads/leads.component';
import { AddleadComponent } from './addlead/addlead.component';
import { AssignedleadsComponent } from './assignedleads/assignedleads.component';
import { LeadteamComponent } from './leadteam/leadteam.component';
import { AllotmentteamComponent } from './allotmentteam/allotmentteam.component';
import { ServiceteamComponent } from './serviceteam/serviceteam.component';
import { AcceptedleadsComponent } from './acceptedleads/acceptedleads.component';
import { FollowupleadsComponent } from './followupleads/followupleads.component';
import { CompletedleadsComponent } from './completedleads/completedleads.component';

import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
 name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
 transform(items: any[], field: string, value: string): any[] {
   if (!items) return [];
   return items.filter(it => it[field] == value);
 }
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserComponent,
    DialogComponent,
    LoginComponent,
    LeadsComponent,
    AddleadComponent,
    AssignedleadsComponent,
    LeadteamComponent,
    AllotmentteamComponent,
    ServiceteamComponent,
    AcceptedleadsComponent,
    FollowupleadsComponent,
    CompletedleadsComponent,

    SearchFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes),
    SidebarModule,
    NavbarModule,
    FooterModule,
    FixedPluginModule,
    BrowserAnimationsModule,
    Ng2SmartTableModule,
    Ng2SearchPipeModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),

  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
