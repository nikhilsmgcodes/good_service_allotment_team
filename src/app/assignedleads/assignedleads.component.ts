import { Component, OnInit } from '@angular/core';
import { Jsonp } from '@angular/http';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-assignedleads',
  templateUrl: './assignedleads.component.html',
  styleUrls: ['./assignedleads.component.css']
})
export class AssignedleadsComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error :any;
  allLeadsDetail : any;
  leadsDetailsById : any ={category:null, brand:null,description:null,date:null};
  customerAddressByLeadId : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  l_id:any;
  service_team : any = {name:null, email:null, st_phone:null, photo:null};
  //public tableData1: TableData;
  public showDialog : boolean;
  public showDialogUpdate : boolean;
  followUpLeadsDetail:any;
  reOpenLeadsDetail:any;
  reOpenFollowUpLeads:any;
  followupmsg:any;
  showFollowUpModel:boolean;
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    this.assignedLeads();
    this.followUpLeads('ASSIGNED',1,0);
    this.reOpenedLeads('ASSIGNED',0,1);
    this.reOpenedFollowUpLeads('ASSIGNED',1,1);
    this.showDialog = false;
    this.showDialogUpdate = false;
  }

  public viewFollowUpLead(l_id){
    this.showFollowUpModel = true;
    this.viewLead(l_id);
    this.getFollowUpInfo(l_id);
  }

  public viewLeadInfo(l_id){
    this.showDialog = true;
    this.viewLead(l_id);
  }

  public reOpenedFollowUpLeads(status,followup,reopen){
    this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          this.reOpenFollowUpLeads=null;
          //alert(data.message);
        }
        else{
          this.reOpenFollowUpLeads=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }

    });
  }

  public assignedLeads(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'getAllAssignedLeadsAdmin',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          this.allLeadsDetail=data.data;
          alert(data.message);
        }
        else{
          this.allLeadsDetail=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }

    });
  }

  public reOpenedLeads(status,followup,reopen){
    this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          this.reOpenLeadsDetail=null;
          //alert(data.message);
        }
        else{
          this.reOpenLeadsDetail=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }

    });
  }

  public followUpLeads(status,followup,reopen){
      this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            this.followUpLeadsDetail=null;
            //alert(data.message);
          }
          else{
            this.followUpLeadsDetail=data.data;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }

    public viewLead(l_id){
      this.l_id = l_id;
      this.data = "l_id="+l_id;
      //this.showDialog = true;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              //alert(data.message);
              //console.log(data);
              this.leadsDetailsById = data.lead_data[0];
              this.customerAddressByLeadId = data.customer_address[0];
              this.getServiceTeamInfoByLeadId()

            };
          }

      });
    }

    public getServiceTeamInfoByLeadId(){
      //this.category = this.leadsDetailsById.category;
      //this.data = "category="+this.leadsDetailsById.category;
      this.data = "l_id="+this.leadsDetailsById.l_id;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'getServiceTeamInfoByLeadIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              this.service_team = data.service_team_info[0];
              console.log(data);
            };
          }

      });
    }

  public getFollowUpInfo(l_id){
      this.data = "l_id="+l_id;

      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getFollowUpInfoAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.followupmsg=data.msg;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
  }


  public accepted(l_id,st_phone){
    let accepted = confirm("Lead Accepted By Service Team member?");
    if(!accepted){

    }
    else{
    this.data = "l_id="+l_id+"&st_phone="+st_phone;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'leadAcceptedAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            alert(data.message);
            this.assignedLeads();
            this.followUpLeads('ASSIGNED',1,0);
            this.reOpenedLeads('ASSIGNED',0,1);
            this.reOpenedFollowUpLeads('ASSIGNED',1,1);
            //this.followUpLeads('ASSIGNED',1,0);
          };
        }
    });
   }
  }


  public unAssignLead(l_id){
    let unassign = confirm("Sure you want to unAssign the Lead?");
    if(!unassign){

    }
    else{
    this.data = "l_id="+l_id;
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
    this.http.post('http://'+this.url+'unAssignLeadAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            alert(data.message);
            this.assignedLeads();
            this.followUpLeads('ASSIGNED',1,0);
          };
        }
    });
   }
  }


}
