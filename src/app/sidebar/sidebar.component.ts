import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $:any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon: 'ti-panel', class: '' },

     { path: 'leads', title: 'Open Leads',  icon:'ti-eye', class: '' },
     { path: 'assignedleads', title: 'Assigned Leads',  icon:'ti-check', class: '' },
     { path: 'acceptedleads', title: 'Accepted Leads',  icon:'ti-check', class: '' },
     { path: 'followupleads', title: 'FollowUp Leads',  icon:'ti-check', class: '' },
     { path: 'completedleads', title: 'Completed Leads',  icon:'ti-check', class: '' },
    //  { path: 'addlead', title: 'Add Lead',  icon:'ti-plus', class: '' },
    //  { path: 'leadteam', title: 'Leads Team',  icon:'ti-user', class: '' },
    //  { path: 'allotmentteam', title: 'Allotment Team',  icon:'ti-user', class: '' },
    //  { path: 'serviceteam', title: 'Service Team',  icon:'ti-user', class: '' },

];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    constructor(public router: Router){

    }
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return true;
    }
    logout(){
      localStorage.clear();
      sessionStorage.removeItem('reload');
      this.router.navigate(['/login']);
    }

}
