import { Component, OnInit } from '@angular/core';
import { Jsonp } from '@angular/http';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

// declare interface TableData {
//     headerRow: string[];
//     dataRows: string[][];
// }

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css']
})
export class LeadsComponent implements OnInit {
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error :any;
  allLeadsDetail : any;
  leadsDetailsById : any ={category:null, brand:null,description:null,date:null};
  customerAddressByLeadId : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  service_team:any;
  selected_st_phone:any;
  l_id:any;
  //public tableData1: TableData;
  public showDialog : boolean;
  public showDialogUpdate : boolean;
  showDialogEdit:boolean;
  category = ['AC','TV','WASHING-MACHINE','REFRIGERATOR','CHIMNEY','WATER-PURIFIER','MICROWAVE-OVEN','GEYSER'];
    selectedCategory : any;
  brand = ['LG','SONY','SAMSUNG','IFB','WHIRLPOOL','BOSCH','SIEMENS','VIDEOCON','ONIDA'];
    selectedBrand : any;
  description:any;
  showFollowUpModel:boolean;
  followupmsg:any;
  followUpLeadsDetail:any;
  reOpenedLeadsDetail:any;
  followUpReOpenedLeads:any;
  constructor(public http: Http, public router: Router) { }
  //constructor(public router: Router){ }

    ngOnInit(){
      this.allLeads();
      this.followUpLeads("OPEN",1,0);
      this.reOpenedLeads('OPEN',0,1);
      this.reOpenedFollowUpLeads('OPEN',1,1);
      this.showDialog = false;
      this.showDialogUpdate = false;
      this.showFollowUpModel = false;
    }

    public viewLead(l_id){
      this.showDialog = true;
      this.viewLeadDetails(l_id);
    }

    public viewFollowUpLead(l_id){
      this.showFollowUpModel=true;
      this.viewLeadDetails(l_id);
      this.getFollowUpInfo(l_id);
    }

    public reOpenedFollowUpLeads(status,followup,reopen){
      this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            this.followUpReOpenedLeads=null;
            //alert(data.message);
          }
          else{
            this.followUpReOpenedLeads=data.data;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }

    public allLeads(){
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.get('http://'+this.url+'getAllOpenLeadsAdmin',this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            alert(data.message);
          }
          else{
            this.allLeadsDetail=data.data;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }

    public followUpLeads(status,followup,reopen){
      this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            this.followUpLeadsDetail=null;
            //alert(data.message);
          }
          else{
            this.followUpLeadsDetail=data.data;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }

    public reOpenedLeads(status,followup,reopen){
      this.data = "status="+status+"&followup="+followup+"&reopen="+reopen;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded',
          'user-phone': JSON.parse(localStorage.getItem('user_phone')),
          'session-key': JSON.parse(localStorage.getItem('session_key')),
          'role': localStorage.getItem('role')
        })
      };
      this.http.post('http://'+this.url+'getAllFollowUpLeadsAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.router.navigate(['/login']);
            alert(data.message);
          }
        else{
          if(data.error){
            this.reOpenedLeadsDetail=null;
            //alert(data.message);
          }
          else{
            this.reOpenedLeadsDetail=data.data;
            //alert(data.message);
            //console.log(this.allLeadsDetail);
          }
        }

      });
    }

    public viewLeadDetails(l_id){
      this.l_id = l_id;
      this.data = "l_id="+l_id;
      //this.showDialog = true;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              //alert(data.message);
              //console.log(data);
              this.leadsDetailsById = data.lead_data[0];
              this.customerAddressByLeadId = data.customer_address[0];
              this.getServiceTeamByCategory()

            };
          }

      });
    }

    public getServiceTeamByCategory(){
      //this.category = this.leadsDetailsById.category;
      this.data = "category="+this.leadsDetailsById.category;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'getServiceTeamByCategoryAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              this.service_team = null;
              this.selected_st_phone = null;
              alert(data.message);
            }
            else{
              //alert(data.message);
              this.service_team = null;
              this.selected_st_phone = null;
              console.log(data);
              this.service_team = data.service_team;
              //this.showDialogUpdate = true;
            };
          }

      });
    }

    public getFollowUpInfo(l_id){
        this.data = "l_id="+l_id;

        this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
        this.http.post('http://'+this.url+'getFollowUpInfoAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              this.followupmsg=data.msg;
              //alert(data.message);
              //console.log(this.allLeadsDetail);
            }
          }

        });
    }


    public deleteLead(l_id){
      let del = confirm("Sure you want to Delete the Lead?");
      if(!del){
        //do nothing
      }
      else{
      this.data = "l_id="+l_id;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'deleteOpenLeadByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              alert(data.message);
              this.allLeads();
            };
          }

      });
     }
    }


    public assignLead(){
      this.data = "l_id="+this.l_id+"&c_phone="+this.customerAddressByLeadId.c_phone+"&st_phone="+this.selected_st_phone;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'assignLeadAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              alert(data.message);
              this.sendMessage();
              this.showDialog = false;
              this.showFollowUpModel = false;
              this.followUpLeads("OPEN",1,0);
              this.reOpenedLeads('OPEN',0,1);
              this.reOpenedFollowUpLeads('OPEN',1,1);
              this.allLeads();
            };
          }

      });
    }

    public editLead(l_id){
      this.l_id = l_id;
      this.data = "l_id="+l_id;
      this.showDialogEdit = true;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'getLeadDetailByIdAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              this.leadsDetailsById = data.lead_data[0];
              this.customerAddressByLeadId = data.customer_address[0];
              this.selectedCategory = this.leadsDetailsById.category;
              this.selectedBrand = this.leadsDetailsById.brand;
              this.description = this.leadsDetailsById.description;
            };
          }
      });
    }

    public updateLeadDetailsById(){
      this.data = 'l_id='+this.leadsDetailsById.l_id+'&category='+this.selectedCategory+'&brand='+this.selectedBrand+'&description='+this.description;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
      this.http.post('http://'+this.url+'updateLeadDetailsById', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.sessionExpire){
              this.router.navigate(['/login']);
              alert(data.message);
            }
          else{
            if(data.error){
              alert(data.message);
            }
            else{
              alert(data.message);
              this.selectedCategory = '';
              this.selectedBrand = '';
              this.description = '';
              this.showDialogEdit = false;
              this.allLeads();
            }
          }
      });
    }

    public sendMessage(){
      this.data = "l_id="+this.l_id+"&st_phone="+this.selected_st_phone+"&ca_id="+this.customerAddressByLeadId.ca_id;
      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded',
          })
        };
      this.http.post('http://'+this.url+'send_message', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
            if(data.error){
              alert(data.message);
            }
            else{
              alert(data.message);
            }

      });
    }
}
