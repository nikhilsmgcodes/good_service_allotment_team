import { Component, OnInit } from '@angular/core';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error:any;

  phone:any;
  password:any;


  constructor(public http: Http,private router: Router) { }

  ngOnInit() {
    this.checkSession();
  }

  checkSession(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'checkSessionAdmin',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.error){
        this.router.navigate(['/login']);
      }
      else{
        this.router.navigate(['/dashboard']);
        //window.location.reload();
      }
  })
}

  doLogin(){
    this.data = "user_phone="+this.phone+'&user_pass='+this.password+'&role='+"allotmentTeam";

    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
    this.http.post('http://'+this.url+'adminLogin', this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.error){
          alert(data.message);
        }else{
          localStorage.setItem('session_key', JSON.stringify(data.session_key));
          localStorage.setItem('user_phone', this.phone);
          localStorage.setItem('role', "allotmentTeam");
          //localStorage.setItem('tableName', 'admin');
          this.router.navigate(['/dashboard']);
        }
      });
  }

}
