import { Component, OnInit, ViewChild } from '@angular/core';
import { Jsonp } from '@angular/http';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addlead',
  templateUrl: './addlead.component.html',
  styleUrls: ['./addlead.component.css']
})
export class AddleadComponent implements OnInit {
//  @ViewChild('addLeadFrom') myForm: any;
  model:any={};
  httpOptions : any;
  posts:any;
  data:any;
  url : string = 'softbizz.in/goodservice/api/public/';
  c_phone : any;
  c_address : any;
  category = ['AC','TV','WASHING-MACHINE','REFRIGERATOR','CHIMNEY','WATER-PURIFIER','MICROWAVE-OVEN','GEYSER'];
    selectedCategory : any;
  brand = ['LG','SONY','SAMSUNG','IFB','WHIRLPOOL','BOSCH','SIEMENS','VIDEOCON','ONIDA'];
    selectedBrand : any;
  description : any;
  selectedAddress = null;
  showDialog : boolean;
  c_name : string;
  c_email : string;
  c_adrs : string;
  c_city : string;
  c_state : string;
  c_zip : string;
  c_optional_phone : any;
  showDiv:boolean;
  showDialogNoCustomer:boolean;
  //c_data : any = {name:null, email:null};
  constructor(public http: Http, public router: Router) { }

  ngOnInit() {
    //this.selectedBrand = 'LG';
  }

  public searchByPhone(){
    //var cphone = this.c_phone.toString();
    if(this.c_phone.toString().length==10){
    this.data = "c_phone="+this.c_phone;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getCustomerAddressByPhoneAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
          this.c_address = null;
          this.showDiv = false;
          if(data.message=='Customer Data Not Found'){
            this.showDialogNoCustomer = true;
            this.c_name = null;
            this.c_email = null;
          }
          else{
            //do nothing
          }
        }
        else{
          this.c_address=data.data;
          //this.c_data = data.c_data[0];
          this.c_name = data.data[0].name;
          this.c_email = data.data[0].email;
          //alert(data.message);
          console.log(this.c_address);
          this.showDiv = true;
        }
      }

    });
   }
   else{
     this.c_address=null;
     this.showDiv = false;
   }
  }

  public addNewLead(){
    //let lt_phone = JSON.parse(localStorage.getItem('user_phone'));
    let ca_id = this.selectedAddress;
    let category = this.selectedCategory;
    let brand = this.selectedBrand;
    let description = this.description;

    this.data = {c_phone:this.c_phone,ca_id:ca_id,category:category,brand:brand,description:description};

    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/json',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'addNewLeadAdmin', this.data,  this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        if(data.error){
          alert(data.message);
        }
        else{
          alert(data.message);
        //  this.myForm.nativeElement.value = '';
          this.c_phone = null;
          this.c_address = null;
          this.selectedCategory = null;
          this.selectedBrand = null;
          this.description = null;
          this.showDiv = false;
        }
      }

    });
  }

  public addNewAddress(){
    this.showDialog = true;
  }

  public addAddress(){
    this.data = {c_phone:this.c_phone,name:this.c_name,email:this.c_email,address:this.c_adrs,city:this.c_city,state:this.c_state,zip:this.c_zip,optional_phone:this.c_optional_phone};
    this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/json',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          })
        };
        this.http.post('http://'+this.url+'addNewCustomerAddressAdmin', this.data, this.httpOptions).map(res => res.json()).subscribe(
            data => {
              if(data.sessionExpire){
                  this.router.navigate(['/login']);
                  alert(data.message);
                }
              else{
                if(data.error){
                  alert(data.message);
                }
                else{
                  alert(data.message);
                  this.showDialog = false;
                  this.searchByPhone();
                  //this.c_phone = null;
                  this.c_name = null;
                  this.c_email = null;
                  this.c_adrs = null;
                  this.c_city = null;
                  this.c_state = null;
                  this.c_zip = null;
                  this.c_optional_phone = null;
                }
              }
            });
  }

}
