import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllotmentteamComponent } from './allotmentteam.component';

describe('AllotmentteamComponent', () => {
  let component: AllotmentteamComponent;
  let fixture: ComponentFixture<AllotmentteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllotmentteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllotmentteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
