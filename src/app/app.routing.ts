import { Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { UserComponent }   from './user/user.component';
import { LoginComponent } from './login/login.component';
import { LeadsComponent } from './leads/leads.component';
import { AddleadComponent } from './addlead/addlead.component';
import { AssignedleadsComponent } from './assignedleads/assignedleads.component';
import { LeadteamComponent } from './leadteam/leadteam.component';
import { AllotmentteamComponent } from './allotmentteam/allotmentteam.component';
import { ServiceteamComponent } from './serviceteam/serviceteam.component';
import { AcceptedleadsComponent } from './acceptedleads/acceptedleads.component';
import { FollowupleadsComponent } from './followupleads/followupleads.component';
import { CompletedleadsComponent } from './completedleads/completedleads.component';



export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'user',
        component: UserComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'leads',
        component: LeadsComponent
    },
    {
        path: 'assignedleads',
        component: AssignedleadsComponent
    },
    {
        path: 'acceptedleads',
        component: AcceptedleadsComponent
    },
    {
        path: 'followupleads',
        component: FollowupleadsComponent
    },
    {
        path: 'completedleads',
        component: CompletedleadsComponent
    }

]
