import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';

declare var $:any;

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
//import {  Platform } from 'ionic-angular';
import { HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{
  model:any={};
  httpOptions : any;
  posts:any;
  data:string;
  url : string = 'softbizz.in/goodservice/api/public/';
  error :any;


  details : any = {openLeads:null,assignedLeads:null};

  constructor(public http: Http, public router: Router) { }


    ngOnInit(){
        this.getInfoForDashboard();

      //   let win = (window as any);
      // if(!sessionStorage.getItem('reload')) {
      //     sessionStorage.setItem('reload','reload')
      //     win.location.reload();
      // }
    }

     // JOB POST Count
  public getInfoForDashboard(){

    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'adminDashboard',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.router.navigate(['/login']);
          alert(data.message);
        }
      else{
        this.details=data;
        console.log(this.details);
     }
    });
  }


}
