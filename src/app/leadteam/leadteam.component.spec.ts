import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadteamComponent } from './leadteam.component';

describe('LeadteamComponent', () => {
  let component: LeadteamComponent;
  let fixture: ComponentFixture<LeadteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
